import React from 'react';
import {useGlobalContext} from '../context/context';
const Alert = () => {
    const {error: {message}} = useGlobalContext();
    return (
        <div id="alert" className="alert">
            {message}
        </div>
    );
}

export default Alert;
