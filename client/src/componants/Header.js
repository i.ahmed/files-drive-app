import React from "react";
import Logo from "../img/logo.png";
import { Link, useHistory } from "react-router-dom";
import { useGlobalContext } from "../context/context";
const Header = () => {
  const { logout, removeAccount, clearLocalState } = useGlobalContext();
  const history = useHistory();
  return (
    <nav>
      <div className="logo-wrapper">
        <img src={Logo} className="brand" />
      </div>
      <ul className="sidenav">
        <li className="active">
          <Link to="/">home</Link>
        </li>
        <li>
          <Link to="/profile">profile</Link>
        </li>
        <li>
          <a
            href="#"
            onClick={async (e) => {
              e.preventDefault();
              const response = await logout();
              if (response.status === 200) {
                clearLocalState();
                history.replace("/login");
              }
            }}
          >
            logout
          </a>
        </li>
        <li>
          <a
            href="#"
            onClick={async (e) => {
              e.preventDefault();
              const response = await removeAccount();
              if (response.status === 200) {
                clearLocalState();
                history.replace("/signup");
              } else {
                alert("Opps! some think went wrong");
              }
            }}
          >
            Delete Account
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default Header;
