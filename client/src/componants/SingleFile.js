import React from "react";
import { BsCloudDownload } from "react-icons/bs";
import { AiOutlineDelete } from "react-icons/ai";
import { useGlobalContext } from "../context/context";

const SingleFile = ({ shortextention, filename, _id, bgcolor }) => {
  const { handleActions } = useGlobalContext();
  return (
    <div className="file">
      <h4 className="extention" style={{ background: bgcolor }}>
        {shortextention}
      </h4>
      <div className="detials">
        <p>
          {" "}
          {`${
            filename.length > 20 ? filename.substr(0, 20) + "..." : filename
          }`}{" "}
        </p>
      </div>
      <div className="actions">
        <span>
          <BsCloudDownload
            id="download"
            data-file-id={_id}
            onClick={(e) => handleActions(e, filename)}
            size="1.4rem"
          />
        </span>
        <span>
          <AiOutlineDelete
            id="delete"
            onClick={(e) => handleActions(e, filename)}
            data-file-id={_id}
            size="1.4rem"
          />
        </span>
      </div>
    </div>
  );
};

export default SingleFile;
