import React from "react";

const Filters = ({sortByRef, limitRef, filter}) => {
  return (
    <div className="actions">
      <select name="limit" ref={limitRef} onChange={filter}>
        <option value="8">8</option>
        <option value="16">16</option>
        <option value="32">32</option>
      </select>
      <select name="sortBy" ref={sortByRef} onChange={filter}>
        <option value="-createdAt">Last created</option>
        <option value="+createdAt">First Created</option>
      </select>
    </div>
  );
};

export default Filters;
