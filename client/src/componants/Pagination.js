import React from "react";
import {AiOutlineArrowLeft, AiOutlineArrowRight} from 'react-icons/ai';

const Pagination = ({paginate, skip}) => {
  return (
    <div className="pagination">
      <button className={`${skip === 0 ? 'disabled' : ''}`} id="prev" onClick={paginate}>
        <AiOutlineArrowLeft/> prev
      </button>
      <button id="next" onClick={paginate}>
        next <AiOutlineArrowRight/>
      </button>
    </div>
  );
};

export default Pagination;
