import React from "react";
import { useGlobalContext } from "../context/context";
import NoData from "./NoData";
import SingleFile from "../componants/SingleFile";
import Spinner from "../componants/Spinner";

const FilesList = () => {
  const { files, isLoading } = useGlobalContext();
  return (
    <div className="content">
      <div className="files-container">
        {isLoading ? (
          <Spinner />
        ) : files.length > 0 ? (
          files.map((file) => {
            return <SingleFile key={file._id} {...file} />;
          })
        ) : (
          <NoData />
        )}
      </div>
    </div>
  );
};

export default FilesList;
