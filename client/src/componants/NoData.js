import React from "react";
import EmptyImg from "../img/no data.png";

const NoData = () => {
  return (
    <div className="files-container" style={{ justifyContent: "center" }}>
      <img src={EmptyImg} />
    </div>
  );
};

export default NoData;
