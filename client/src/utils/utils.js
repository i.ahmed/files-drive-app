import axios from "axios";
export const login = async (username, password) => {
  return await requests({ username, password }, "/users/login", "POST");
};
export const setLocalState = ({ username, token, activated }) => {
  localStorage.setItem(
    "token",
    JSON.stringify({
      username,
      token,
      activated,
    })
  );
};
export const getLocalState = () => {
  return localStorage.getItem("token");
};
export const clearLocalState = () => {
  localStorage.clear();
};
export const validateData = (fields) => {
  const emptyFields = fields.filter((field) => {
    return field === "";
  });
  return emptyFields.length > 0;
};
export const isLogedIn = () => {
  const user =
    getLocalState("token") !== null ? JSON.parse(getLocalState("token")) : null;
  if (!user || !user.activated) {
    return false;
  }
  return true;
};
export const signup = async ({ username, password, email, image }) => {
  const formData = new FormData();
  formData.set("username", username);
  formData.set("password", password);
  formData.set("email", email);
  if (image !== null) {
    formData.set("image", image);
  }
  return await requests(formData, "/users/signup", "POST", {
    "Content-Type": "multipart/form-data",
  });
};
export const verfiyEmail = async (code, name) => {
  return await requests({ code, name }, "/users/verfiy", "POST");
};
export const logout = async () => {
  const token = JSON.parse(getLocalState())["token"];
  return await requests({}, "/users/logout", "POST", {
    Authorization: `Bearer ${token}`,
  });
};
export const showProfile = async () => {
  const token = JSON.parse(getLocalState())["token"];
  return await requests({}, "/users/profile/show", "GET", {
    Authorization: `Bearer ${token}`,
  });
};
export const updateProfile = async ({
  username,
  email,
  password,
  image = null,
}) => {
  const token = JSON.parse(getLocalState())["token"];
  const formData = new FormData();
  formData.set("username", username);
  formData.set("email", email);
  if (password !== "") {
    formData.set("password", password);
  }
  if (image !== null) {
    formData.set("image", image);
  }
  console.log(formData.get("image"));
  return await requests(formData, "/users/profile/update", "PATCH", {
    Authorization: `Bearer ${token}`,
    "Content-Type": "multipart/form-data",
  });
};
export const removeAccount = async () => {
  const token = JSON.parse(getLocalState())["token"];
  return await requests({}, "/users", "DELETE", {
    Authorization: `Bearer ${token}`,
  });
};
export const loadFiles = async ({
  limit,
  sortBy: { field, order },
  skip = 0,
}) => {
  const token = JSON.parse(getLocalState())["token"];
  return await requests(
    {},
    `/files?limit=${limit}&sortBy=${field}&order=${order}&skip=${skip}`,
    "GET",
    {
      Authorization: `Bearer ${token}`,
    }
  );
};
export const uplodeFiles = async (files) => {
  const formData = new FormData();
  const token = JSON.parse(getLocalState())["token"];
  for (let i = 0; i < files.length; i++) {
    formData.append("files", files[i]);
  }
  return await requests(formData, "/files", "POST", {
    Authorization: `Bearer ${token}`,
    "Content-Type": "multipart/form-data",
  });
};
export const updateFile = async (file, id) => {
  const token = JSON.parse(getLocalState())["token"];
  const formData = new FormData();
  formData.set("file", file);
  return await requests(formData, `/files/${id}`, "PATCH", {
    Authorization: `Bearer ${token}`,
    "Content-Type": "multipart/form-data",
  });
};
export const removeFile = async (id) => {
  const token = JSON.parse(getLocalState())["token"];
  return await requests({}, `/files/${id}`, "DELETE", {
    Authorization: `Bearer ${token}`,
  });
};
export const downloadFile = async (fid, filename) => {
  const token = JSON.parse(getLocalState())["token"];
  return await requests(
    {},
    `/files/${fid}`,
    "GET",
    {
      Authorization: `Bearer ${token}`,
    },
    true,
    filename
  );
};
export const requests = async (
  data,
  route,
  method,
  headers = {},
  isFile = false,
  filename = ""
) => {
  try {
    let config = { method, url: route, data };
    if (headers) {
      config["headers"] = headers;
    }
    if (isFile) {
      config["responseType"] = "blob";
    }
    const response = await axios(config);
    if (isFile) {
      const downloadUrl = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = downloadUrl;
      link.setAttribute("download", filename); //any other extension
      document.body.appendChild(link);
      link.click();
      link.remove();
      return;
    }
    return { status: response.status, ...response.data };
  } catch (e) {
    return { status: e.response.status, ...e.response.data };
  }
};
