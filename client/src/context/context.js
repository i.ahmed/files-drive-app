import React, { useContext, useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import * as utils from "../utils/utils";

const AppContext = React.createContext();
const AppProvider = ({ children }) => {
  const [error, setError] = useState({ status: false, message: "" });
  const [files, setFiles] = useState([]);
  const [refresh, setRefresh] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [user, setUser] = useState({
    username: "",
    password: "",
    email: "",
    token: "",
    activated: false,
    activationCode: "",
    image: [],
  });

  const handleActions = (e, filename) => {
    const source = e.target.id;
    const fid = e.target.dataset["fileId"];
    switch (source) {
      case "download":
        utils.downloadFile(fid, filename);
        break;
      case "delete":
        utils.removeFile(fid);
        setRefresh(!refresh);
        break;
    }
  };
  useEffect(() => {
    const loggedInUser =
      utils.getLocalState() !== null ? JSON.parse(utils.getLocalState()) : {};
    setUser({ ...user, ...loggedInUser });
  }, []);
  return (
    <AppContext.Provider
      value={{
        ...utils,
        user,
        setUser,
        error,
        setError,
        files,
        setFiles,
        refresh,
        setRefresh,
        isLoading,
        setIsLoading,
        handleActions,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};
const useGlobalContext = () => {
  return useContext(AppContext);
};
export { AppProvider, useGlobalContext };
