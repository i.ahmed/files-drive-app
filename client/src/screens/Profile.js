import React, { useState, useEffect } from "react";
import { useGlobalContext } from "../context/context";
import UserImg from "../img/user.png";

const Profile = () => {
  const { showProfile, updateProfile, user, setUser, refresh, setRefresh } =
    useGlobalContext();

  const fetchProfile = async () => {
    const response = await showProfile();
    setUser({
      ...user,
      username: response.username,
      email: response.email,
      image: response.image,
    });
  };
  const handleChange = (e) => {
    const key = e.target.name;
    const value = e.target.value.trim();
    setUser({ ...user, [key]: value });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await updateProfile({ ...user });
    if (response.status === 200) {
      setRefresh(!refresh);
      alert("profile updated");
    } else {
      alert(`Opps! ${response.error}`);
    }
  };
  useEffect(() => {
    fetchProfile();
  }, [refresh]);
  return (
    <section className="wrapper">
      <div className="container">
        <img
          src={`${
            Array.isArray(user.image) && user.image.length > 0
              ? `data:${user.image[0].extention};base64,${user.image[0].image}`
              : UserImg
          }`}
          alt=""
        />
        <form id="profile-form" className="profile" onSubmit={handleSubmit}>
          <div className="input-wrapper">
            <input
              type="email"
              name="email"
              placeholder="Enter your email"
              value={user.email}
              onChange={handleChange}
              required
            />
            <span className="required">*</span>
          </div>
          <div className="input-wrapper">
            <input
              type="text"
              name="username"
              placeholder="Enter your username"
              value={user.username}
              onChange={handleChange}
              required
            />
            <span className="required">*</span>
          </div>
          <div className="input-wrapper">
            <input
              type="password"
              name="password"
              placeholder="Enter your new password"
              value={user.password}
              onChange={handleChange}
            />
            <span className="required">*</span>
          </div>
          <div className="input-wrapper">
            <input
              type="file"
              onChange={(e) => {
                if (e.target.files.length > 0) {
                  setUser({ ...user, image: e.target.files[0] });
                }
              }}
            />
          </div>
          <button type="submit">Update</button>
        </form>
      </div>
    </section>
  );
};

export default Profile;
