import React from "react";
import { useHistory } from "react-router";
import Alert from "../componants/Alert";
import { useGlobalContext } from "../context/context";
import Logo from "../img/logo.png";
const Activate = () => {
  const { verfiyEmail, getLocalState, error, setError, user, setUser } =
    useGlobalContext();
  const history = useHistory();
  const handleSubmit = async(e) => {
    e.preventDefault();
    const {username} = JSON.parse(getLocalState());
    const response = await verfiyEmail(user.activationCode , username);
    if(response.status == 200){
        history.push('/');
    }else{
       setError({ ...error, status: true, message: response.error });
    }
  };
  return (
    <div className="wrapper">
      <div className="container">
        <img src={Logo} alt="" />
        <form id="verification-form" onSubmit={handleSubmit}>
          <div className="input-wrapper">
            <input
              type="text"
              placeholder="Enter your verification code"
              value={user.activationCode}
              onChange={(e) =>
                setUser({ ...user, activationCode: e.target.value.trim() })
              }
              required
            />
            <span className="required">*</span>
          </div>
          <button type="submit">verifiy</button>
          {error.status && <Alert />}
        </form>
      </div>
    </div>
  );
};

export default Activate;
