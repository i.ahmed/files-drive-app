import React, { useEffect, useState, useRef } from "react";
import FilesList from "../componants/FilesList";
import Filters from "../componants/Filters";
import Header from "../componants/Header";
import Pagination from "../componants/Pagination";
import { useGlobalContext } from "../context/context";
const Home = () => {
  const { loadFiles, setFiles, refresh, setIsLoading, uplodeFiles } =
    useGlobalContext();
  const [skip, setSkip] = useState(0);
  const sortByRef = useRef();
  const limitRef = useRef();
  const fileRef = useRef();

  const fetchFiles = async () => {
    setIsLoading(true);
    const response = await loadFiles({
      limit: limitRef.current.value.trim(),
      sortBy: {
        order: sortByRef.current.value.trim().substr(0, 1),
        field: sortByRef.current.value
          .trim()
          .substr(1, sortByRef.current.value.length),
      },
      skip,
    });
    setIsLoading(false);
    if (response.status === 200) {
      fileRef.current.value = "";
      setFiles(response.files);
    } else {
      alert("Opps! some think went wrong");
    }
  };
  const filter = async () => await fetchFiles();
  const paginate = async (e) => {
    const source = e.target.id;
    if (e.target.className.indexOf("disabled") === -1) {
      if (source === "next") {
        setSkip((value) => {
          value += parseInt(limitRef.current.value.trim());
          return value;
        });
      } else {
        setSkip((value) => {
          value -= parseInt(limitRef.current.value.trim());
          return value;
        });
      }
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const filesWrapper = fileRef.current.files;
    if (filesWrapper.length > 0) {
      const response = await uplodeFiles(filesWrapper);
      if (response.status === 201) {
        await fetchFiles();
      } else {
        alert(`Opps! ${response.message}`);
      }
    }
  };
  useEffect(() => {
    fetchFiles();
  }, [skip, refresh]);
  return (
    <div className="container-fluid">
      <Header />
      <div className="container">
        <form id="uplode-form" onSubmit={handleSubmit}>
          <div className="input-wrapper">
            <input type="file" id="files" required multiple ref={fileRef} />
            <span className="required">*</span>
          </div>
          <button type="submit">Upode</button>
          <Filters sortByRef={sortByRef} limitRef={limitRef} filter={filter} />
        </form>
      </div>
      <Pagination paginate={paginate} skip={skip} />
      <FilesList />
    </div>
  );
};

export default Home;
