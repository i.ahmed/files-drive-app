import React from "react";
import { useHistory, Link } from "react-router-dom";
import Logo from "../img/logo.png";
import { useGlobalContext } from "../context/context";
import Alert from "../componants/Alert";
const Login = () => {
  const { validateData, setLocalState, login, error, setError, user, setUser } =
    useGlobalContext();
  const history = useHistory();
  const handleChange = (e) => {
    const key = e.target.name;
    const value = e.target.value.trim();
    setUser({ ...user, [key]: value });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validateData([user.username, user.password])) {
      setError({
        ...error,
        status: true,
        message: "Please Fill Required Fields",
      });
    } else {
      const response = await login(user.username, user.password);
      if (response.status === 200) {
        const currentUser = {
          username: response.user.username,
          token: response.token,
          activated: response.user.activated,
        };
        setLocalState(currentUser);
        setUser({ ...user, ...currentUser });
        setError({ ...error, status: false, message: "" });
        history.push("/");
      } else {
        setError({ ...error, status: true, message: response.error });
      }
    }
  };
  return (
    <div className="wrapper">
      <div className="container">
        <img src={Logo} alt="" />
        <form id="login-form" onSubmit={handleSubmit}>
          <div className="input-wrapper">
            <input
              type="text"
              name="username"
              placeholder="Enter your username"
              required
              value={user.username}
              onChange={(e) => handleChange(e)}
            />
            <span className="required">*</span>
          </div>
          <div className="input-wrapper">
            <input
              type="password"
              name="password"
              placeholder="Enter your password"
              required
              value={user.password}
              onChange={(e) => handleChange(e)}
            />
            <span className="required">*</span>
          </div>
          <button type="submit">Login</button>
          {error.status && <Alert />}
          <div className="signup-section">
            If your do not have an account create one
            <Link to="/signup"> here</Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
