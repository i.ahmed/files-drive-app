import React from "react";
import { useHistory, Link } from "react-router-dom";
import Alert from "../componants/Alert";
import Logo from "../img/logo.png";
import { useGlobalContext } from "../context/context";
const Signup = () => {
  const {
    error,
    setError,
    signup,
    validateData,
    user,
    setUser,
    setLocalState,
  } = useGlobalContext();
  const history = useHistory();
  const handleChange = (e) => {
    const key = e.target.name;
    const value = e.target.value.trim();
    setUser({ ...user, [key]: value });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validateData([user.username, user.password, user.email])) {
      setError({
        ...error,
        status: true,
        message: "Please Fill Required Fields",
      });
    } else {
      const response = await signup({
        email: user.email,
        username: user.username,
        password: user.password,
        image: user.image || null,
      });
      if (response.status === 201) {
        const currentUser = {
          username: response.user.username,
          token: response.token,
          activated: response.user.activated,
        };
        setLocalState(currentUser);
        setUser({ ...user, ...currentUser });
        setError({ ...error, status: false, message: "" });
        history.push("/activate");
      } else {
        setError({ ...error, status: true, message: response.error });
      }
    }
  };
  return (
    <div className="wrapper">
      <div className="container">
        <img src={Logo} alt="" />
        <form id="signup-form" onSubmit={handleSubmit}>
          <div className="input-wrapper">
            <input
              type="email"
              name="email"
              placeholder="Enter your email"
              value={user.email}
              onChange={handleChange}
              required
            />
            <span className="required">*</span>
          </div>
          <div className="input-wrapper">
            <input
              type="text"
              name="username"
              placeholder="Enter your username"
              value={user.username}
              onChange={handleChange}
              required
            />
            <span className="required">*</span>
          </div>
          <div className="input-wrapper">
            <input
              type="password"
              name="password"
              placeholder="Enter your password"
              value={user.password}
              onChange={handleChange}
              required
            />
            <span className="required">*</span>
          </div>
          <div className="input-wrapper">
            <input
              type="file"
              name="image"
              value={user.image}
              onChange={handleChange}
            />
          </div>
          <button type="submit">signup</button>
          {error.status && <Alert />}
          <div className="login-section">
            If already have an account login <Link to="/login">here</Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Signup;
